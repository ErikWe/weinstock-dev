---
layout: ../../../layouts/layout_blog.astro
title: Did Ubuntu Touch already fail me?
date: 2023-02-20 18:00:00
tags:
  - Tech
  - Ubuntu Touch
  - Metcalfe's law
---

My mind is racing in circles as I am questioning the decision I made today. Four months ago, I switch from an Android phone to a german made Ubuntu Touch phone. Today, I switched back to Android. This short blog reveals the reason behind it and it is not what you expect.

A bit background at the beginning. Since years, I am trying to reduce my digital footprint to a minimum. In this context, I deleted my Facebook account years ago, I switch from WhatsApp to Signal and later from Signal to Matrix. I removed my LinkedIn, Reddit and Pr0gramm accounts and when it comes to gaming, I deleted most of my accounts there as well. The latter came at a higher price as I lost all the games I purchased at these platforms. Yet, I was more and more satisfied how things developed and then I made the biggest cut. I moved from Android to Ubuntu Touch.
There are countless reasons for and against such a change but in the end, I was fine with it. I replaced my last Android dependencies with alternatives e.g. dedicated hardware tan generators for my online banking.

Ubuntu Touch worked fine. The basic functionality is covered thats for sure. The rest is rather lame but I can live with it. Lets check it:

- Performance was not comparable with other phones but hey, it was ok
- Apps were not as good as on Android but hey, they were ok
- Other features were not present but hey, the basics worked ok

This does not sound bad, so why the sudden change?
In the end, it is _Metcalfe's_ law which describes the reason pretty good. I just joined a diving club in Karlsruhe and they have two _WhatsApp_ groups. I absolutely do not want to miss these groups. Actually I need them, at least in the beginning as long as I am new. And thus, I switched to Android again. And yes, I switched to WhatsApp again. Still, I want to continue to use Signal for most stuff but offering WhatsApp to my friends and family gives them a new, more comfortable platform than SMS and E-Mail. And now I am literally standing at the beginning of my journey. &#129318

Lession learned: **As long as I am not financially in the position to dictate my rules, I have to have fallbacks and sometimes, I need to deviate from my plans, at least for a short amount of time**.

However, there are two positive points about this:

1. I learned a lot about Ubuntu Touch and while it is not the standard now, I am sure it will play a major roll, sooner or later. And thus, I will continue to use the phone in my free time and I will try to contribute further to the project.
2. I always loved my WhatsApp status and now I can use it again: "Zeit ist Geld! Dies bitte zu bedenken und im Kontakt mit mir sich möglichst einzuschränken."
