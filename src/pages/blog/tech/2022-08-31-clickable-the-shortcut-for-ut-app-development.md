---
layout: ../../../layouts/layout_blog.astro
title: Clickable - The shortcut for UbuntuTouch app development
date: 2022-08-31 18:00:00
tags:
  - Tech
  - Ubuntu-Touch
  - Clickable
---

As I am rather new to UbuntuTouch and especially its app development, I had to take a closer look and found something which is worth to mention. The internet in its beauty is usually full of s\*\*\* but in it, there is this one sparkling thing called _Clickable_. [Clickable](https://clickable-ut.dev/en/latest/index.html) describes itself with the following words:

"Build and compile Ubuntu Touch apps easily from the command line. Deploy your apps to your Ubuntu Touch device for testing or test them on any desktop Linux distribution. Get logs for debugging and directly access a terminal on your device."

And as easy as it sounds, as easy it is - at least when you get it to run. This is why the following blog contains an installation- and a quick usage-guide.

### Installation:

First, make sure that Docker is installed via apt and **not via Snap** as the Snap version has insufficient access. Second, run the following four commands:

`sudo apt install docker.io adb git python3 python3-pip python3-setuptools`

`pip3 install --user clickable-ut --upgrade`

`echo 'export PATH="$PATH:$HOME/.local/bin"' >> ~/.bashrc`

`pip3 install --user git+https://gitlab.com/clickable/clickable.git@dev`

Restart your terminal and finish the setup with `clickable setup`. This will finalize the setup process.

**Note:** if you experience any errors during the setup or usage and you have an Nvidia graphics card, then please install the [_nvidia-container-toolkit_](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html).

### How to use Clickable:

Clickable eases the development of UbuntuTouch apps by providing various features. The ones I am using frequently are the following:

- `clickable create` to create a new UbuntuTouch app from a predefined template
- `clickable build` to build the project
- `clickable ide qtcreator` to run the QT-Creator IDE
- `clickable desktop` to run the app in an emulator on my desktop
- `clickable devices` to find my VollaPhone22
- `clickable shell` to display and access a shell running on my phone
- `clickable launch` to run the local app on my phone for testing

As you can see, Clickable offers an all-in-one package with absolutely useful commands and tools. But just offering tools does not make me this satisfied about a product right? Yeah, you are kind of right. I did not tell the hole truth as it even gets better. If you check the commands again, you may ask yourself, how everything works and if it would work on your machine as well. Not every machine is setup the same way and these different environments could lead to the classic development headaches. The solution is called Docker and Clickable really lives this containerization approach as almost every command pulls its own container with everything setup and ready to go. If you have some space for several images with approximately 6 GB, your are good to go and prepared to give the app development a try **(even without an UbuntuTouch phone)**.
