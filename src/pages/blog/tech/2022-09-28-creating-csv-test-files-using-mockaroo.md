---
layout: ../../../layouts/layout_blog.astro
title: Creating CSV test files using Mockaroo
date: 2022-09-28 19:39:00
tags:
  - Tech
  - CSV
  - Mockaroo
---

Because of my [dive-planning-slate](https://gitlab.com/ErikWe/dive-planning-slate) project which can be used to calculate required gases/tanks for a predefined dive, I was in need for CSV files which store a bunch of dive steps. These dive steps are basically triples built from a _depth_, a _duration_ and the related _gas_ during this step. An example basic dive step triple looks like this (20,15,N21) while a decompression triple looks like this (6,10,N99).

Now, there are two ways to create these files:

1. Manually create each one of them
2. Use a tool to create them

At this point, I know exactly that you know, what I was doing &#129327. Of course, because I was lazy as f\*\*\*, I was looking for a nice tool which may generate what I want without me typing countless entries like a monkey. But I did not expect a solution as the one I found. And the solution is called [Mockaroo](https://mockaroo.com/). This website is the absolute _one-punch-man_ when it comes to CSV generation. For my use case, I entered these settings:

- Field Name: depth; Type: Number; Options: min=1, max=50, decimals=0, blank=0%
- Field Name: duration; Type: Number; Options: min=1, max=20, decimals=0, blank=0%
- Field Name: gas; Type: Custom List; Options: (N21,N32,N99),random,blank=0%

In addition, I selected 20 rows and removed the check from the _header_ checkbox to prevent the field names being displayed in the CSV and clicked on preview.
The result is quite nice:

- 28,20,N21
- 36,9,N32
- 22,4,N32
- 23,5,N99
- 6,13,N21
- 4,2,N99
- 47,18,N32
- 8,20,N99
- 33,20,N32
- ...

But why do you write a blog entry about such basic CSV files? Because Mockaroo is able to create much more complex combination and also allows the creation of _invalid entries_ in which values are missing. It also allows the the generation of random email addresses and so much more.
After playing around a bit, I created a list of 1000 entries in the following format:

- id,first_name,last_name,email,gender,ip_address
- 1,Shane,Duffield,sduffield2@smugmug.com,Female,123.73.212.37
- 2,Artemas,Markel,amarkel3@google.ca,Male,29.48.75.231
- ...

If I need to create other CSV files for testing purposes, this will be where I would look first!
