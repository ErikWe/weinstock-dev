---
layout: ../../../layouts/layout_blog.astro
title: Pimp my Firefox
date: 2022-12-30 21:00:00
tags:
  - Tech
  - Firefox
  - Extensions
---

A friend recently asked me what extensions (Add-ons) I am currently using on Firefox. This is why I thought a new blog post would be suitable for that. Especially when I look at the date of my last technical post &#129488.
Now, lets go right to my list of installed Firefox extensions:

1. **HTTPS Everywhere** (Deprecated): An extension which I really like is _HTTPS Everywhere_ which enforces HTTPS connections by rewriting all requests to a given site to HTTPS. In January 2023 however this functionality will be part of Firefox itself. This is why I marked this extension as deprecated.
2. **Decentraleyes**: Operates as a local cache for libraries which are usually provided by third parties. This increases you privacy and reduces noise on your network.
3. **uBlock Origin**: Guess you have this one already installed. Basically _uBlock Origin_ is an ad-blocker which, in my humble opinion, does a very good job.
4. **Privacy Badger**: I stumbled across this extension during work, when I had to manually edit and monitor cookies. If you want to get rid of trackers, start using this extension as it also learns to identify 'invisible' trackers and beats them up.
5. **NoScript**: Use _NoScript_ to block scripts and scripted websites. [TOR](https://support.torproject.org/glossary/noscript/) already got it pre-installed. So why not using it in your day-to-day browser?
6. **No Gender**: I actually wanted to make this blog non-political but as everything is political today, I guess I will not find a way around it. _No Gender_ is an extension I use to get rid of all these gender specific stuff on websites to simply increase readability and shorten the overall text on these pages.

These six extensions are my basic setup. I usually play with some more but I mostly remove them after some time when I am not satisfied with them.
Feel free to contact me if you find additional add-ons/extensions which you can recommend...
