---
layout: ../../../layouts/layout_blog.astro
title: Monitoring with Munin
date: 2023-03-20 18:00:00
tags:
  - Tech
  - Munin
  - Monitoring
---

A few days ago, I set up [Munin](https://munin-monitoring.org/) for my device monitoring. This blog post shows how I did it and why Munin is a must have tool when it comes to system data collection.

My basic setup contains a Raspberry Pi 3 with a 16 GB SD card as master to collect, store and display all my metrics. All the other servers in my network are running as Munin nodes. The following example shows the basic setup for the master and two nodes (my other Raspberry Pi's) but it can be used as a template for adding additional nodes as well.

**On the master:** On the master, make sure to run `sudo apt-get install munin` and add the following entries into the `/etc/munin/munin.conf` config file:

```
# a simple host tree
[rpi0.globaltrust.com]
    address 192.168.1.20
    use_node_name yes

[rpi1.globaltrust.com]
    address 127.0.0.1
    use_node_name yes

[rpi2.globaltrust.com]
    address 192.168.1.22
    use_node_name yes

[<node name>.<node domain>]
    address <static node IP>
    use_node_name yes

```

Make sure that your master has the localhost (127.0.0.1) address configured in order to get the metrics form your master as well. After you saved your config file, you do not need to restart the service as it will be used at the next poll automagically.

**On a node:** Setting up a node is easy. But make sure you add it to the master config as well as (checkout the config above). In your machine, run the following command: `sudo apt-get install munin-node` and edit the `/etc/munin/munin-node.conf` by adding the _static_ master IP to it:

```
# A list of addresses that are allowed to connect.  This must be a
# regular expression, since Net::Server does not understand CIDR-style
# network notation unless the perl module Net::CIDR is installed.  You
# may repeat the allow line as many times as you'd like

allow ^127\.0\.0\.1$
allow ^192.168.1.21$
```

After saving this change, restart the service with `sudo service munin-node restart` and after the next poll of your master, the node should be displayed in the web-interface.

If you are like me and you are running **Apache > 2.4**, please got to `/etc/munin/apache24.conf` and refactor it to look like this:

```
<Directory /var/cache/munin/www>
    Require all granted
    Options FollowSymLinks SymLinksIfOwnerMatch
</Directory>

<Directory /usr/lib/munin/cgi>
    Require all granted
    Options FollowSymLinks SymLinksIfOwnerMatch
    <IfModule mod_fcgid.c>
        SetHandler fcgid-script
    </IfModule>
    <IfModule !mod_fcgid.c>
        SetHandler cgi-script
    </IfModule>
</Directory>
```

At this place I want to thank j7nn7k who described this fix in [Stackoverflow](https://stackoverflow.com/a/12058057).

**This was it?** Yes, this was it. Now, the master is continuously polling metrics from all configured hosts. Metrics which are not present due to hardware constraints are simply left blank. It is super easy and having to invest this little amount of time to get a detailed overview of what is going on in your homelab is a super deal. I can advice anyone to give it a try.

And if the web-interface does not look as good as you like it, you can display the data in Grafana as well...
