---
layout: ../../../layouts/layout_blog.astro
title: Proxmox as my new virtualization platform
date: 2023-03-19 14:00:00
tags:
  - Tech
  - Proxmox
  - Virtualization
---

**"Everything in this world is magic...except to the magician."** - _Dr. Robert Ford (Westworld)_

A wise quote from one of the best acting performances I have ever witnessed. It applies to so many things but when it comes to computer science, this quote really describes this domain. One part which always fascinated me was the virtualization. Everything, from simple applications until whole operating systems, inspired me.
Thus, I always wanted to have a virtualization platform as a playground in my homelab. And because of this wish, I was looking out for a suitable solution for me until I found [Proxmox](https://www.proxmox.com/en/proxmox-ve). A platform made in Austria &#127462&#127481 which provides everything I need. An integration of a KVM hypervisor and Linux Containers (LXC) with additional features such as easy snapshot and backup creation, state of the art web interface and countless possibilities to use it.

I guess you have two questions now, where do I run it and what am I running on it?

**Where do I run it?**
Because there was no hardware suitable for me, I simply build my own server... again. I used the same 4U case and put the following components into it:

- Intel Core i5 10400 6x 2.90GHz So.1200 BOX
- ASRock H470M-HDV/M.2 DDR4 µATX So.1200 retail
- 32GB G.Skill Aegis DDR4-2666 DIMM CL19 Dual Kit
- 400 Watt be quiet! Pure Power 11 Non-Modular 80+ Gold
- 512GB Silicon Power P34A80 M.2 PCIe 3.0 x4 3D-NAND TLC
- be quiet! Shadow Rock LP Topblow Cooler
- WD Red NAS Hard Drive 4TB

This setup was put together within 30 minutes. The only challenging part was to install the CPU cooler as it required some calm and precise finger movements to fixate the screws, which I was really struggling with. However, the installation of Proxmox was super smooth and since two weeks, it is running without any issues... NICE!

**What do I run on it?**
As it is a playground, I started to setup some basic VMs just to get used to it. From time to time, I moved other systems in my network into Proxmox. Currently running on my Proxmox are the following applications:

- Unifi Controller
- Heimdall
- Jellyfin
- ArchiSteamFarm

But I am planning to host much more applications such as Grafana, Audiobookshelf, Dillinger, Jenkins and Huginn, just to name a few. The overall integration into my networks was fine and it super easy to use. For now, I cannot give a detailed review on Proxmox and its Pro's and Con's but I have the feeling that I will have quite some fun with this new component in my homelab.

And while some parts are still magic to me, I am working on myself to become a magician in the near future. &#129497
