---
layout: ../../../layouts/layout_blog.astro
title: Protontricks - Next level Wine
date: 2023-01-24 07:00:00
tags:
  - Tech
  - Steam
  - Protontricks
  - Wine
---

Well, it is not a secret that I prefer Linux based operating systems. More than a year ago, I deleted my Microsoft account and installed Ubuntu on all my machines, including my gaming machine. Nowadays, Steam comes with Proton, an approach to run games developed for Microsoft Windows on a Linux machine. This works astonishingly well and I rarely experience problems.

But, today I was not able to start Factorio because of some shader issues and I had to do some manual tinkering which I want to document in this short blog.
Updating the graphics card driver did not help as well as executing the game with the latest Proton version. So, I checked on [protondb.com](https://protondb.com) if there is some comment of players who ran into the same issue. And indeed, I found a comment about Protontricks but I had no clue what it is until now.

Protontricks is kind of an interface between Proton and Wine and can be seen as the Proton pendant to Winetricks. It allows you to install additional packages and .dll files to properly mimic a Windows machine. So, what did I do?

First, I installed Protontricks:

```
sudo apt install python3-pip python3-setuptools python3-venv pipx
pipx install protontricks
```

Second, I was looking for my Factorio installation and the game ID (427520):

```
protontricks -s Factorio
```

Third, I told Protontricks to install the _d3dcompiler_47_ for my Factorio:

```
protontricks 427520 d3dcompiler_47
```

This basically tells Winetricks to install the compiler and to use it on the next start of the game.
And like there has never been any problem, it starts immediately and runs smoothly.

Overall, if you are fine with tinkering like this once or twice a year, Linux with Steam will be your friend. (And you can finally kill your last remaining Windows machine which you only used for gaming...)
