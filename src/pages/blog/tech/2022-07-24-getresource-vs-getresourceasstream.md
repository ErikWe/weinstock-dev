---
layout: ../../../layouts/layout_blog.astro
title: getResource() vs. getResourceAsStream()
date: 2022-07-24 13:00:00
tags:
  - Tech
  - Java
  - Resources
---

Today I stumbled upon a problem which was easy to fix but quite interesting to see. The task was to find a way in Java to read a file from the classpath (_src/resources/filename.png_). As easy as it sounds, as easy it is. Using `Objects.requireNonNull(getClass().getClassLoader().getResource("filename.png"))` will return a _URI_ which in turn can be easily transformed into a _File_ object. Testing it in the IDE works fine. Problem solved right?

Sure, you guessed it, it was not solved at all. While it works inside the IDE, it did not work anymore when executing the packaged _.jar_ file.
So, searching for a solution gave me a &#129318 as it is actually obvious. When executing the program in my IDE, all the resources are in the correct place and in the correct format. However, using _Maven_ to package the project to a working _.jar_ file will move all the resources to the root folder of the _jar_. In addition, the resulting _jar_ is a byte stream instead of a collection of standard files. The _jar_ itself is the only file.

So, how to fix it? Well, now that we know this fact, we are using _getResourceAsStream()_ as displayed in the following snippet:

`Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("filename.png"))`

This approach has no problem working in the IDE and as _jar_. In fact, it is also easier to integrate this implementation into the rest of the code because most of the other methods can operate with _InputStreams_ right away such as `ImageIO.read(inputStream)` which I used to store the image as _BufferedImage_.
