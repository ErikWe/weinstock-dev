---
layout: ../../../layouts/layout_blog.astro
title: Sdkman solves all your SDK problems
date: 2022-08-14 17:00:00
tags:
  - Tech
  - SDKMAN
  - SDK
  - JVM
---

The story of every software developer out there contains at some point the installation and set-up of a SDK (Software Development Kit). And going further, I believe that most developers did experience trouble immediately or at least soon after they installed their SDK. There are many reasons which may lead to difficulties such as installing the wrong SDK, the wrong version or dependencies which require newer or older versions. In addition, the SDK may be installed in a non-default location on the users system or is simply not detect after the installation procedure is completed. In the end, no developer likes to install them but almost all developers have to install and maintain them.

And as you can imagine, I myself had to face all of the aforementioned problems at least once. But, some time ago, I figured out that out there, a bunch of developers built a solution for this problem. And as I am facing the SDK problem again, I thought about sharing the solution in this blog. The solution is called [SDKMAN!](https://sdkman.io/). On their website, SDKMAN is summarized as follows: \"_SDKMAN!_ is a tool for managing parallel versions of multiple _Software Development Kits_ on most Unix based systems. It provides a convenient Command Line Interface (CLI) and API for installing, switching, removing and listing Candidates.\". And this is exactly what they do. But how to use it?

On Linux and MacOS, you can install SDKMAN with `curl -s "https://get.sdkman.io" | bash`. Let's say, you need Maven installed. Use `sdk list maven` to see all available Maven versions. Install the latest version (3.8.6) with `sdk install maven 3.8.6`. Maven is installed and ready to use. If you type `mvn --version` in a new terminal, you should get the following or a similar output:

```
Apache Maven 3.8.6 (84538c9988a25aec085021c365c560670ad80f63)
Maven home: /home/erik/.sdkman/candidates/maven/current
Java version: 17.0.3, vendor: Oracle Corporation, runtime: /home/erik/.sdkman/candidates/java/17.0.3-oracle
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "5.15.0-46-generic", arch: "amd64", family: "unix"
```

Now, assume that you need a lower version, then simply use `sdk install maven 3.8.5` to install it. To use it, enter the following command: `sdk use maven 3.8.5`. This will enable the older version inside the terminal you are executing the command in. If you want to set the older version system wide for each new terminal, then use the following command: `sdk default maven 3.8.5`.
This example works for many more SDK's such as Groovy, Java, Kotlin, Scala and Tomcat, just to name a few. If you need more help, visit [sdkman.io](https://sdkman.io/) or use `sdk help` to get more CLI options.
