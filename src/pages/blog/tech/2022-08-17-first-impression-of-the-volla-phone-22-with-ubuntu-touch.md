---
layout: ../../../layouts/layout_blog.astro
title: First impression of the Volla Phone 22 with Ubuntu Touch
date: 2022-08-17 07:00:00
tags:
  - Tech
  - Volla-Phone-22
  - Ubuntu-Touch
---

Yesterday, my new phone was delivered and as the title spoilers, it is a [Volla Phone 22](https://volla.online/de/) with Ubuntu Touch pre-installed. There are several reasons for my decision but I guess the three most important ones are the need for a new phone, the production in Germany and more importantly, less Google. The fact that it is a Linux-based operating system just made it more interesting for me.

However, after some delivery problems I was glad to finally hold it in my hands. The hardware is fine, as it is basically a Gigaset GS5. One nice feature is an exchangeable battery which most smartphone vendors do not offer nowadays. But let's talk about the Ubuntu Touch which is the main reason for this blog-entry.
The OS is actually very intuitive and runs rather fine on the Volla Phone 22. However, there are quite a bunch of design decisions which I cannot understand right now...

- The Clock app does show the time, a timer and a stopwatch on top of the app. This is absolutely fine but the app is also capable of setting an alarm. However, instead of adding this functionality to the top bar, you have to swipe up in order to see the alarm setting. &#128686
- I can use the CalDAV protocol to sync my calendar but not my contacts. &#129313
- The apps for Signal or other important platforms such as Spotify or Plex are all rather lame and very restrictive in their functionality. &#129327

As you can see, the phone and the operating system is absolutely not recommendable for the average consumer. **But**, Ubuntu Touch is also very open. And this is where the fun begins. While the current apps are rather useless, the overall platform allows me to build the apps and tools which I need. And maybe, if I find the time, I will be able to support the current project and maybe move the alarm setting into the top control bar.

My motivation is still high and I will evaluate this experiment in the following weeks. I am curious where this road leads me and what I will do with this great opportunity.
