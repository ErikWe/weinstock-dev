---
layout: ../../../layouts/layout_blog.astro
title: Why Euroboxes are the solution to all problems
date: 2022-10-21 09:00:00
tags:
  - Non-Tech
  - Standards
  - Euroboxes
---

Some days ago, I invited my cousin for a traditional veal sausage breakfast with some pretzels and beer. Because my main room is literally without furniture, his eyes stumbled across my brewing equipment and of course my stack of euroboxes. Now, the funny thing is that he was not confused about the emptiness of the room or my tools but rather by my excessive use of euroboxes. Thinking about it, I am glad that he was only able to find the one in the main room as I own more euroboxes than single pieces of furniture. However, he somehow managed to make pictures of it and for the rest of the day, I had to justify myself why using euroboxes is the superior and logical way to store ones belongings. But why am I using all of them and for what?

Now, everybody who really knows me, knows that I am a kind of obsessed with _order_ and _standards_ which for me are just one way to achieve order. Luckily euroboxes are standardized. They come in a variety of sizes but no matter what size, they all stick together like Lego bricks. The usual size is 600x400 mm or 400x300 mm which can be perfectly aligned on a 1200x800 mm euro-pallet. This fact is the root of most benefits of these boxes and a blessing for the industry as well as for the consumers.

But why are these boxes so superior and what makes them so versatile applicable? Let me show you some points:

1. Euroboxes are cheap. A stack of boxes costs around 200 € while a wardrobe, cabinet or cupboard is usually more expensive.
2. Euroboxes are stackable. Instead of scaling storage space horizontally, euroboxes store things primarily vertically. This means that the same storage space requires less area from the apartment compared to a traditional wardrobe.
3. Euroboxes have a neutral design. While furniture is nowadays rather bought as a design element instead of a storage element, euroboxes with their simple design fit in every apartment. Even when moving to a new one, euroboxes are no visual highlight but not ugly at all.
4. Euroboxes ease the move into a new apartment. When moving to a new apartment, I do not need to pack my stuff as 80% of my belongings is already stored in euroboxes which can be moved right away.
5. Euroboxes can be moved. While a wardrobe cannot be moved easily, euroboxes which are stacked upon wheels can be moved super easy. This allows better cleaning (behind the boxes) but also to increase the usable space in a room if I require it because I can roll all my boxes into another room.
6. Euroboxes are versatile. I do not have a bathtub to clean larger things but with euroboxes, I can simply place an empty one into my shower and use it as a huge bucket or a miniature bathtub. This trick helps me to clean a huge amount of empty beer bottles before refilling them.
7. Euroboxes merge with the surrounding. Because of the defined standard, these boxes can be merged with all sorts of things e.g. gastronorm boxes or beer crates. Everything can be stacked and they usually fit into furniture.
8. Euroboxes are waterproof. While some people think that this is not important, they should try to store things in a cardboard box in a wet basement. Long story short, the cardboard boxes will not last long.
9. Euroboxes are strong. Because of their structure and the material they are made of, euroboxes are very strong and can be loaded with heavy stuff as well.
10. Euroboxes create order. This might be a personal point but for me, euroboxes create order. I am using these boxes to create a clear separation between different context types. When I am looking for something, I immediately find it.

While these benefits may be understandable, you may ask what I am using these boxes for? Well, I can give you another list:

1. Boxing equipment
2. Diving equipment
3. Climbing/Hiking equipment
4. Brewing equipment
5. Building blocks (Lego/Cobi)
6. Electrical components
7. Cables
8. Tools for cleaning my shoes
9. Socks and underwear
10. Books
11. Washing powder
12. Baking ingredients and equipment
13. Waste sorting
14. Cleaning supplies

As you can see, I am using euroboxes for most of my stuff and I never regret the decision to do so. They create order, look nice and they provide a certain level of flexibility. But I need to be honest. If there is one thing which bothers me sometimes, it is the fact that stacking these boxes creates a FILO (**F**irst **I**n **L**ast **O**ut ) structure. Depending on the weight of the boxes, two or more can be replaced at once but it is still not optimal. However, if there would be a solution without some negative points, everybody would use it, right?

Now go ahead and make fun about me and my boxes but the next time you are not finding something, you need to clean behind your wardrobe, you need to move your furniture to a new apartment, you are in rage because your cardboard boxes broke or the next time you need to pack something, think about this approach and maybe give it a try.

Trust me, euroboxes are the superior way to store your stuff.
