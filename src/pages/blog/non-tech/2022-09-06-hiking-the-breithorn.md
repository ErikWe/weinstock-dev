---
layout: ../../../layouts/layout_blog.astro
title: Hiking the Breithorn
date: 2022-09-06 09:00:00
tags:
  - Non-Tech
  - Hiking
  - Breithorn
  - Zermatt
---

Last Sunday I took some friends and went to the Breithorn (4164 m) in Zermatt Switzerland. The goal sounds easy, just get on the Breithorn and back again. Therefore the following plan was created:

1. Get up early (1:30 AM) and drive to Täsch
2. Park the car, get equipped and go to the train station
3. Drive from Täsch to Zermatt by train
4. Walk from the Zermatt train station to the lift station
5. Use the lift to go to Furi -> Schwarzsee -> Trockener Steg -> Klein Matterhorn
6. On top of the Klein Matterhorn (Matterhorn Glacier Paradise), get prepared and setup the 'rope team'
7. Get up to the Breithorn
8. Get back to the lift before 4 PM
9. Reverse the arrival steps

Having this plan on our side and our tools, food and tickets prepared in advance, we were ready to go. The drive to Täsch was quite fast without any major problems. Even the ride through the Lötschberg tunnel was fast. However, we arrived in Täsch with already one hour delay and tried to speed up a bit. The train to Zermatt was nice but not that impressive. If Zermatt would not be a car-free town, we would not have taken the train...
From the train station to the lift station was a nice walk through the center of the town, allowing us to take some pictures. Especially the Matterhorn was looking nice in the sunlight. We went up to the Klein Matterhorn and out on the glacier. After preparing our rope team, we walked through the snow from the day before towards the summit. Half-way through, we put on our crampons and continued. While the overall route is very easy, the height should not be underestimated. We walked as fast as our endurance allowed us to go but the clouds were faster. Even though we started in sunshine with a blue sky, we ended up in a stormy wind inside a huge cloud. However, it was an absolute exciting experience to walk the final meters on the ridge of the Breithorn.
As time went by so fast, we had to move quick in order to get to the lift station before they close. We finally made it in time and went back to the car, packed our stuff and drove back to Germany. We arrived at 11:30 PM tired and exhausted but with a backpack full of nice memories and a small sense of pride.

Now that we know how it works, where to drive and what to to, the most important question is still not answered. How much did it actually cost?
The following table lists all costs in Euro:

<center>
| Item                                    | Price         |
|-----------------------------------------|---------------|
| Highway Toll                            | 41 €          |
| Lötschberg Train Ticket                 | 2x 30 €       |
| Täsch 1 Day Parking Ticket              | 17 €          |
| Train Between Täsch and Zermatt         | 2x 8.50 €     |
| Matterhorn Glacier Paradise Lift Ticket | 111 €         |
| Gas (Seat Ibiza)                        | approx. 100 € |
|                                         |               |
| **Total**                               | **346 €**     |
</center>
