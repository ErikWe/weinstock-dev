---
layout: ../../../layouts/layout_blog.astro
title: One month with the 9-Euro-Ticket. What's coming next?
date: 2022-07-03 16:00:00
tags:
  - Non-Tech
  - Traffic
  - 9-Euro-Ticket
---

When the 9-Euro-Ticket was introduced, I was excited about the overall project. The idea is simple, pay 9€ and travel with Train, Bus, and other means of transport for one whole month. The even better part, it is valid in all of Germany. In my whole life I only had a few offers which seemed more beneficial than that. So, the assumption would be that this offer changes many things. And obviously it caused me to travel more. But was this really the case?
Now, after one month with this 'golden ticket', I have to say that it did not change that much. Overall, I met my friends in a bar once, I drove to a friend's BBQ night when it was about to rain and I did some shopping in the city. To and from sport, I went by bicycle, I did my grocery shopping by bicycle and anything else by foot.

In the end, I saved some money using the 9-Euro-Ticket and it allowed me to attend some events which I would not visit without it. But the expected change in my behavior has not occurred.
Am I going to buy it again? YES, I absolutely am! In this case, it is not about driving more by train, tram, or something else. My bike is perfectly fine, and I love riding it. But it is about sending a message, that this concept of 'pay one price and travel with more or less every type of transportation' is the future. I really enjoyed the flexibility and the options I had. And for me, this kind of flexibility is worth more than these 9€...
