---
layout: ../../../layouts/layout_blog.astro
title: Trying the Klarstein Brauheld Pro for the first time
date: 2022-08-02 07:00:00
tags:
  - Non-Tech
  - Brewing
  - Brauheld-Pro
  - Pilsner
---

Recently, I added the Klarstein Brauheld Pro 30 L to my brewing equipment in order to increase the amount of beer produced in one brewing session. After unpacking and cleaning it, it was time to try it. For the first test run, I decided to brew an easy Pilsner beer. The reason for this decision is the easy recipe and fermentation as it is a bottom-fermented beer. The following tables list the ingredients and recipe for the so-called "_Allgäu-Bio-Pils_".

<center>
| Ingredients                                               | Amount   | Price   |
|-----------------------------------------------------------|----------|---------|
| Weyermann Bio Pilsner Malz (2,5 - 4,5 EBC)                | 6 kg     | 20,10 € |
| Bio Tettnanger Aromahopfen Pellets 4,9 % Alpha Ernte 2021 | 100 g    | 8,35 €  |
| Saflager W-34/70 bottom-fermenting dry yeast              | 2x11,5 g | 9,98 €  |
</center>

<center>
| Steps | Temperature | Duration  | Comment      |
|-------|-------------|-----------|--------------|
| Step1 | 55 °C       | 10 min    | Mashing      |
| Step2 | 55 °C       | 30 min    |              |
| Step3 | 62 °C       | 20 min    |              |
| Step4 | 72 °C       | 20 min    |              |
| Step5 | 78 °C       | 20 min    | Lautering    |
| Step6 | 98 °C       | 90 min    | Wort boiling |
| Step7 | 20 °C       | 30-60 min | Wort cooling |
| Step8 | 10-12 °C    | 2.5 weeks | Fermenting   |
</center>

As you can see, the ingredients and the steps are quite simple. It is worth to mention that I added the hops in the following order: 1/10 at the beginning, 5/10 after 15 min and 4/10 after 60 min. Now, that the overall process is clear, how did it go with the Brauheld Pro?

Actually, it was quite a mess. Even though my version of the Brauheld should be suited for 30 liters and I was aiming vor 22-25 liters, the malt in this recipe was way too much. In the end, it went into the space between the heater and the malt reservoir which caused several side-effects. Removing some of the malt from this reservoir and adding a bit more water did fix the problem temporarily. Later after lautering, I decided to try a hops-sock in order to keep all the hops particles separated from the wort. The goal was a more "filtered" beer and less particles inside the pump system. But, the malt from the beginning already clogged the filter in front of the pump so that everything was blocked. There was no circulation anymore and I had to improvise and had to stir the wort by hand. It was working quite ok but I do not spend this much money on a machine that is just not well designed. Another problem appeared when filling the cooled wort into the fermentation tank. As the output of the Brauheld boiler is also the input for the pump, the same clogged filter was now blocking the wort from flowing into the fermentation tank. I had to disinfect my arm and grab into the wort in order to remove the filter from the system.

As you can see, despite choosing a simple recipe and beer, the whole process was not smooth at all. But now, the _young-beer_ is stored inside the tank in my refrigerator at constant 10°C and everything seems to be ok.

Overall, I cannot give the Brauheld a good rating for now. But I learned several lessons:

- Reduce the amount of water and with it, the amount of malt required
- Try to reduce the amount of particles in the wort even further (Maybe by the use of a _brew-bag_)
- Use a longer brewing-paddle
- Increase the cooling period as cooling down 25 liters of wort from 98°C to 20°C takes quite some time **and water...**

Let's see how the beer will taste in the end and brew it again while focusing on the errors detected and analyzed this time.
