---
layout: ../../../layouts/layout_blog.astro
title: Top 10 things to do in Naples
date: 2023-08-16 20:30:00
tags:
  - Non-Tech
  - Travel
  - Italy
  - Naples
---

_Vedi Napoli e poi muori_ Blah, blah, blah... I've been to Naples lately. Just one week, nothing special. At least this is what I thought. In the end it was one of my best holidays because of several reasons. One reason is the cool stuff to experience down there. And to get right into it, let me just list them here for you:

My personal top 10 places to visit when being down there:

**Number 10: Pompeii**
Sure, when talking about Naples, Pompeii is always close. By a ticket in advance and use the public transport to get there. Sure, it is nice to walk through this city/site. You will see well preserved buildings, wall paintings and get a nice feeling about the way they lived back then. At least about how the rich people lived. However, overall a must see even though I found other things more interesting. Hint: If you use the train, board it at _Napoli Porta Nolana_ and use the standard Circumvesuviana. First, this is the initial station, most people will board at the next station which is the central station and thus you will get a seat. Second, the express train takes almost as long as the normal train but is five times the price. Just stay in the trenches and take the poor mans train like us &#128516

**Number 9: Mount Vesuvius**
It is rather a mountain than a volcano. Thus, mount Vesuvius sounds better in my opinion. However, if you want to visit it and I would advice you so, buy a ticket online. If you miss that, you walk all the way up in vain. I heard that there is a free way around the mountain which should be possible as well but if you are on a tight schedule as I was, the main way is the way. If you want to go there, book a parking place as well. Otherwise it will be way more challenging. And one last point, try to choose a day with a good visibility.

**Number 8: Funicolare**
_Jammo 'ncoppa, jammo ja',funiculi', funicula'!_ Well, if you are a train enthusiast like me, try the Funicolare which is a super convenient way to get up to _Vomero_ and the _Castel Sant'Elmo_. Very nice experience and rather cheap. Overall, when staying in Naples, try to use the public transportation. It is in good conditions, always clean and very convenient. Regarding punctuality, it definitely can compete with the Deutsche Bahn but Germans know that this is not a real challenge...

**Number 7: Castel Sant'Elmo**
Talking about the Funicolare, I have to name the _Castel Sant'Elmo_. On top of a mountain, it is a rather old fortress. Entry price is only 3,50€ which is totally fair for a view over the whole city. If the conditions are good, you can clearly see mount Vesuvius as well. My tipp, buy a one way train with the Funicolare, visit the Castel and then walk down the mountain by foot to experience even more of Naples. In the end, you will end up in the Spanish Quarters which are also nice to see and very good to find delicious food...

**Number 6: Walk around**
Wait, what is this shit? A top 10 list with one bullet point being "walk around"? Yes, it is. Just walk around and observe the people. Sure, there are tourists but if you see them, choose another road. Look behind the curtain. It is so interesting to see the real people there. The most funny thing I saw rather often is the fact that very often, the people will order stuff from their balcony and pull up their goods with a rope and a "bucket" attached to it. So basically, if they see someone selling water, they let down their bucket with money and the vendor puts the water bottles into it. After the trade was made, they pull up the bucket and got their items. And this my friends are things normal tourists will not see. Check it out, do not stick to public places...

**Number 5: Skulls and Bones**
The people of Naples are very gullible. I guess this is the wrong word as they are actually thinking the right way. However, they have a very strong culture related to skulls of dead people. Some people took real skull into their home and prayed for them. They did it for a reason. It was basically a deal. They will pray for the dead person to get out of the purgatory and in return, the dead person will give them luck. So, not really gullible but rather business people. The result are churches and catacombs related to this cult. Skulls and bones are everywhere. My advice, visit the church _Santa Maria delle Anime del Purgatorio ad Arco_ as well as _The Catacombs of San Gaudioso_. Really interesting to experience.

**Number 4: Subterranean Naples**
Napoli Sotteranea. While most tourists see Naples from the normal sites, there is basically a second city underground. The Greeks were the first to dig holes in order to store water. Later the Romans built a fresh water system and during WW2 those tunnels have been used as bunkers. Tons of stories, sayings and legends are rooted in those tunnels and catacombs. I visited four of them and each one was special and unique. It is among the best things to do when it is hot outside.

**Number 3: Mingle with the locals**
Naples for tourists is a 10 out of 10. But the people of Naples are famous for their "Spirit" as I would describe it. They freed themselves from the fascist regime back in WW2 so that the allies did not really have to attack the city, they have their football team (including Maradona of course) and they have their own dialect. So, they are special and thus I advice you to blend into it. Try to get in contact with the locals, go out and party with them. They are so friendly, open but also proud people that I really enjoyed my time with them. **And please forget about the crime in the city. You will not recognize anything and the people themselves are not that way.**

**Number 2: The food**
Sounds simple and it is simple. While every five star cook around the world tries to make some fusion-hyper-super food, the cooks in Naples do not give two shits about this trend. They love their _mamma_ and she told them that if the use simple but good ingredients, the recipes of their ancestors and do their work in an honorable way, their food will be the best. And again, as simple as it sounds as simple it is. The food in Naples is the best I have ever tasted. Nothing more to add.

**Number 1: Diving in the sunken city of Baia**
Sunken city might be wrong, it was actually more of a SPA area and water park for the emperors from 2000 years ago. Because of the volcanic activity, the ground sunk down about 5-6 meters which caused the park to end up under water. Luckily they found it and now you can dive there. In a depth of 3-5 meters, you will find several mosaic floors, statues and foundations of the buildings. I did this trip with [Centro Sub Campi Flegrei](https://www.centrosubcampiflegrei.it/) and it was absolutely fantastic. Hint: do this at the beginning of your holiday, we booked two days but could only go at one because of the weather and it was our last day...

In the end. Visit Naples. Do not fear the german sayings about people stealing your phone or wallet. Sure, prevent those things in advance by choosing the right outfit but overall, I had zero bad experiences and this is super rare. I always felt save, the people are really, really nice and you will never be hungry. I will promise you that!
