---
layout: ../../../layouts/layout_blog.astro
title: Building my first Cobi model
date: 2022-07-24 09:00:00
tags:
  - Non-Tech
  - Cobi
  - Model
  - Tank
---

Several years ago, the [Held der Steine](https://www.youtube.com/c/HeldderSteine) began producing videos about Lego models. Later he began promoting third-party Lego compatible products/bricks as well. And while Lego has never been affordable for me as a kid and while it is still not affordable for me as a software engineer, Cobi and other manufacturers offered something worth looking at. In addition to the price, which does not favor Lego, there are two other factors which made me buy such a product. First, the production which Cobi does in Europe while others produce abroad (at least to a certain extend). Second, the kind of models they provide. The reason for Lego not to produce military models such as tanks is described in many articles such as this [ZME Science](https://www.zmescience.com/other/did-you-know/lego-military-toys/) article. However, Cobi does sell tanks and other military models.

And thus, I became a customer of Cobi, just to take me back to my childhood. _At least for a couple of hours..._

Now, after building the tank I have to say that I am absolutely satisfied with the model, the quality and the level of detail. My tank, a PzKpfw VI Tiger Ausf. E (Basically the German WWII Tiger Tank), comes in a nice dark-gray color with various features to play with. The top of the tank can be turned 360°, the gun can be moved up and down and the tank track does move when it is pushed forward or backward. In short, the model I always dreamed of as a child.

What did annoy me was an issue with the delivered pieces. Four identical pieces are actually the inverse of four other pieces which haven't been delivered. Now, my model is not fully complete as I am missing four pieces but I also have four other pieces too much.

But this does not really impact my satisfaction as it looks good, it is a tank, it is made in Europe, it is affordable compared to Lego and it is compatible with Lego. The next time, I drive back into my hometown, I will take it with me and store it in my Lego box. One day, someone will find it again and have some fun with it...

...and as far as I know myself, it is quite certain that every time I visit my hometown, I will visit my Cobi model tank as well.
