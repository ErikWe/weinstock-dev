---
layout: ../../../layouts/layout_blog.astro
title: Indoor diving in Siegburg (Dive4Life)
date: 2023-04-14 20:30:00
tags:
  - Non-Tech
  - Diving
  - Indoor
---

Today I took one day PTO, rented a car and drove all the way from Karlsruhe to Siegburg. The reason is the [Dive4Live](https://www.dive4life.de/) indoor diving center. The overall location is super nice. Parking next to the door, elevators are present, sign-in is easy and there is enough space to prepare your gear.
But more important, how is the submerged location? Well, it is nice as well. It is a cylinder with a wider upper part and a more narrow but deeper lower part. Everywhere are statues and artefacts which make your dive feel like diving through a sunken city. The water temperature was nice with 25 °C and the visibility was crystal clear.
My dive buddy, a friend of mine which I met in Thailand, and I did four dives. Each dive was approximately 45 minutes as he dives single tank only. However, to be fair, 45 minutes are quite fine as you will get bored real quick. The art is nice but after two dives you have seen it all.
In the end, I still recommend going there because it is the perfect environment to train some exercises such as smb deployment or laying and following a line.
And finally to be fair, a day pass during workdays costs 45€ only. This includes lead (if you need it) and air. You can even bring your own tanks and refill them after you did your dives.

5 Star location and a super nice experience.
