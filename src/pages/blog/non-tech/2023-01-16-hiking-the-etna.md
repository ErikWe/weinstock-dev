---
layout: ../../../layouts/layout_blog.astro
title: Hiking the Etna
date: 2023-01-16 15:30:00
tags:
  - Non-Tech
  - Hiking
  - Etna
  - Catania
---

As a person who loves Italy, it was always a dream to come to Sicily. The island itself is quite nice and has a lot to offer. This year, I got the ability to visit a friend of mine in Catania which is a the eastern side of the island.

Even though it is January, the weather is perfect with temperatures at around 18 °C. This is why I packed my climbing stuff and flew to Catania as we stayed in a nice house in Carruba which is 45 minutes from the airport. Two days later, we got up at 6 AM, had a decent breakfast and drove right to the Etna, one of the most active volcanoes in Europe.

The plan was simple, park at the entry of the cable car station called Rifugio Sapienza (1900m), walk all the way up to the exit of the cable car (2500m). Using it in January would cost 30€ which was too expensive for me. From this 'summit' station, we followed a road along the eastern side of the Etna visiting _Cratere del Laghetto_ and later the _Torre del Philosopher_. From this point upwards, there was no way, no trail, just some lonely steps scattered in the snow.

We decided to not visit the _Monte Frumento Supino_ but to head north, with the Etna in sight. However, the wind got stronger and temperatures fell below zero which made it extremely exhausting to continue. We managed to get next to the _South Eastern Crater_ and made a small stop. Now, the weather got completely mad. The visibility was at around 10-15 meters, our drinking water was frozen and our phones stopped working. Even the GoPro I took with me did not work. In fact, it was just a ball of solid ice and I was not able to get it working. While the wind got stronger and stronger, we decided to head back as it was simply too dangerous. On our way down, we lost our orientation as our steps were all gone and the visibility got even worse. While keeping our heading to the south, we found the _Torre del Philosopher_ again and thus were able to get back to the road.
As soon as we reached the 'summit' station, we decided to follow the bus road which was longer but it was possible to follow it. And after an hour we were back at the car.

Overall, it was a super nice experience and my first volcano ever. The rocks and the overall nature is so different compared to the Alps. It is possible to go there without a guide and thus save around 100€ per person. But if you decide to go alone, make sure to be prepared for all contingencies. Pack warm clothes and good shoes as the rocks tend to fall apart whenever you step on them.

**Note:** some people say that a guide is mandatory but we did not see a sign and nobody stopped us on our way. To be honest, in mid January there was almost nobody, we started long before the cable car opened and nobody got in our way.
