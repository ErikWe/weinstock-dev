import { h } from "preact";
import Styles from "./styles.module.scss";
import { format } from "date-fns";

function BlogPreview({ entry }) {
  const { frontmatter } = entry;
  return (
    <div className={Styles.card}>
      <div className="pa2">
        <div className={Styles.blogTitle}>
          <a href={entry.url}>
            {format(new Date(frontmatter.date), "yyyy.MM.dd") +
              " - " +
              frontmatter.title}
          </a>
        </div>
        <div className={Styles.tags}>
          Tagged:
          {frontmatter.tags.map((t) => (
            <div className={Styles.tag} data-tag={t}>
              {t}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default BlogPreview;
