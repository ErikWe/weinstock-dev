import { h } from "preact";
import Styles from "./styles.module.scss";

function Nav() {
  return (
    <nav className={Styles.nav}>
      <a className={Styles.logolink} href="/">
        <div className={Styles.monogram}>EW</div>
      </a>
      <a className={Styles.link} href="/">
        Home
      </a>
      <a className={Styles.link} href="https://gitlab.com/ErikWe">
        GitLab
      </a>
      <a className={Styles.link} href="/blog/">
        Blog
      </a>
      <a className={Styles.link} href="/contact/">
        Contact
      </a>
    </nav>
  );
}
export default Nav;
