import { h } from "preact";
import Styles from "./styles.module.scss";

function Footer() {
  return (
    <footer className={Styles.footer}>
      &copy; {new Date().getFullYear()} Erik Weinstock
      <small className={Styles.byline}>
        <a href="https://favicon.io/emoji-favicons/light-bulb/">Favicon</a> |{" "}
        <a href="https://cojdev.github.io/lowpoly/">Background image</a> |{" "}
        <a href="/imprint/">Impressum</a> | 🚀 Built by Astro
      </small>
    </footer>
  );
}
export default Footer;
