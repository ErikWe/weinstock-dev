# Weinstock Dev

This is the repository of my personal website [weinstock.dev](https://weinstock.dev). Feel free to use the code and the setup but without any warranty. The content and images on the other side need to be replaced. You can find the dockerized setup in my [docker-collection](https://gitlab.com/ErikWe/docker-collection) repository.

## Content

- Installation
- Build
- License

## Installation

To install and run this project, you need git, nodejs >= v14 and yarn. The rest will be installed during the setup.

### Install git

```bash
sudo apt update && sudo apt install git
```

### Install node v16

Install curl to download the nodejs setup file:

```bash
sudo apt install -y curl
```

Download and run the nodejs setup file using curl:

```bash
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
```

Install nodejs:

```bash
sudo apt install -y nodejs
```

Check the installed version:

```bash
node --version
```

### Install yarn

In this project, yarn is used as our package manager. Therefore you need to install it via npm.
Use the following command to install it globally:

```bash
npm install --global yarn
```

After a successful install, check the installed version:

```bash
yarn --version
```

### Install packages

To install all the required dependencies/packages, go into the project folder and execute yarn to install all dependencies listed inside the `package.json`

```bash
yarn
```

## Build

### Build resume and project

To build the resume only, run the `yarn run pre-build` command. If you want to build a working website, run `yarn run build`.

## License

weinstock-dev
Copyright (C) 2023 ErikWe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
